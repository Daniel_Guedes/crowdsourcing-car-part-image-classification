﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CarPartImageClassification.Data;
using CarPartImageClassification.Models;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using CarPartImageClassification.ViewModels;

namespace CarPartImageClassification.Controllers
{
    public class ImagesController : Controller
    {
        private readonly CarPartImageClassificationContext _context;
        private readonly IWebHostEnvironment _he;

        public ImagesController(CarPartImageClassificationContext context, IWebHostEnvironment he)
        {
            _context = context;
            _he = he;
        }

        // GET: Images
        public async Task<IActionResult> Index()
        {
            var boundingBoxesAwaitingConfirmation = await _context.Image
                .Include(i => i.RelationHIT)
                .AnyAsync(i => i.RelationHIT.Where(rh => rh.Type == Models.Type.Confirmation && rh.Finished == false).Count() == 0 && i.BoundingBoxLeft != null && i.BoundingBoxIsCorrect == null);
               
            var imagesAwaitingBoundingBoxes = await _context.Image
                .Include(i => i.RelationHIT)
                .AnyAsync(i => i.RelationHIT.Where(rh => rh.Type == Models.Type.Creation && rh.Finished == false).Count() == 0 && i.BoundingBoxLeft == null);

            var imagesAwaitingCreation = !await _context.Image.AnyAsync();

            var hitsInProgress = await _context.Image_HIT
                .AnyAsync(ih => ih.Finished == false);

            return View(new IndexImageViewModel
            {
                Images = await _context.Image.ToListAsync(),
                AwaitingBoundingBox = imagesAwaitingBoundingBoxes,
                AwaitingConfirmation = boundingBoxesAwaitingConfirmation,
                AwaitingCreation = imagesAwaitingCreation,
                HITsInProgress = hitsInProgress
            });
        }

        public async Task<IActionResult> GenerateImages()
        {
            if (await _context.Image.CountAsync() != 0)
                return RedirectToAction(nameof(Index));

            string filePath = Path.Combine(_he.ContentRootPath, "pictures.txt");
            string file = System.IO.File.ReadAllText(filePath);

            string[] imagesData = file.Split("\r\n");
            
            foreach (string imageData in imagesData)
            {
                try
                {
                    string[] imageDataParsed = imageData.Split(" ");

                    var image = new Image
                    {
                        Url = imageDataParsed[0],
                        Classification = imageDataParsed[1] switch
                        {
                            "front_light" => Classificacao.FrontLight,
                            "back_light" => Classificacao.BackLight,
                            "fog_light" => Classificacao.FogLight,
                            "console" => Classificacao.Console,
                            "grill" => Classificacao.Grill,
                            "speedometer" => Classificacao.Speedometer,
                            "instrument_panel" => Classificacao.InstrumentPanel,
                            "gear_handle" => Classificacao.GearHandle,
                            "steering_wheel" => Classificacao.SteeringWheel,
                            _ => throw new NotImplementedException()
                        },
                        ImageLocation = imageDataParsed[2]
                    };

                    _context.Image.Add(image);
                }
                catch (NotImplementedException)
                {
                    continue;
                }           
            }

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        // GET: Images/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var image = await _context.Image
                .Include(i => i.RelationHIT)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (image == null)
            {
                return NotFound();
            }

            return View(new ImageViewModel
            {
                Image = image,
                CreationHITs = image.RelationHIT.Where(rh => rh.Type == Models.Type.Creation).ToList(),
                ConfirmationHITs = image.RelationHIT.Where(rh => rh.Type == Models.Type.Confirmation).ToList()
            });
        }
 
        // GET: Images/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var image = await _context.Image
                .FirstOrDefaultAsync(m => m.Id == id);
            if (image == null)
            {
                return NotFound();
            }

            return View(image);
        }

        // POST: Images/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var image = await _context.Image.FindAsync(id);
            _context.Image.Remove(image);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ImageExists(int id)
        {
            return _context.Image.Any(e => e.Id == id);
        }
    }
}
