﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CarPartImageClassification.Data;
using CarPartImageClassification.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Amazon.MTurk;
using Amazon.MTurk.Model;
using CarPartImageClassification.ViewModels;
using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Drawing;
using Amazon.S3;
using Amazon;
using Amazon.S3.Transfer;

namespace CarPartImageClassification.Controllers
{
    public class HITsController : Controller
    {
        private readonly CarPartImageClassificationContext _context;
        private readonly AmazonMTurkClient _mturkClient;
        private readonly IAmazonS3 _s3Client;
        private readonly IWebHostEnvironment _he;
        private readonly string _bucketName = "boundingboximages";
        private static readonly string URL = "https://mturk-requester-sandbox.us-east-1.amazonaws.com";

        public HITsController(CarPartImageClassificationContext context, IOptions<AWSAuth> awsAuth, IWebHostEnvironment he)
        {
            _context = context;
            _he = he;

            AmazonMTurkConfig config = new AmazonMTurkConfig
            {
                ServiceURL = URL
            };

            _mturkClient = new AmazonMTurkClient(
                awsAuth.Value.AccessKeyID,
                awsAuth.Value.SecretKey,
                config
            );

            _s3Client = new AmazonS3Client(
                awsAuth.Value.AccessKeyID, 
                awsAuth.Value.SecretKey, 
                RegionEndpoint.EUWest3
            );
        }

        // GET: HITs/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
                return NotFound();

            var hitResponse = await _mturkClient.GetHITAsync(new GetHITRequest 
            {
                HITId = id
            });

            var imageRelation = await _context.Image_HIT
                .Include(ir => ir.Image)
                .FirstOrDefaultAsync(ir => ir.MTurkID == id);

            if (imageRelation == null)
                return RedirectToAction("Index", "Images");

            return View(new HITViewModel
            {
                HIT = hitResponse.HIT,
                Image = imageRelation.Image,
                TypeHIT = imageRelation.Type
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AcceptAllAssignments(string id)
        {
            if (id == null)
                return NotFound();

            var assignmentsResponse = await _mturkClient.ListAssignmentsForHITAsync(new ListAssignmentsForHITRequest
            {
                HITId = id,
                AssignmentStatuses = new List<string>() { "Submitted" }
            });

            foreach (var assignment in assignmentsResponse.Assignments)
            {
                await _mturkClient.ApproveAssignmentAsync(new ApproveAssignmentRequest
                {
                    AssignmentId = assignment.AssignmentId
                });
            }

            return RedirectToAction("Details", new { id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AcceptAllHITsAssignments()
        {
            var hits = await _context.Image_HIT.Where(ih => ih.Finished == false).ToListAsync();

            foreach (var hit in hits)
            {
                var assignmentsResponse = await _mturkClient.ListAssignmentsForHITAsync(new ListAssignmentsForHITRequest
                {
                    HITId = hit.MTurkID,
                    AssignmentStatuses = new List<string>() { "Submitted" }
                });

                foreach (var assignment in assignmentsResponse.Assignments)
                {
                    await _mturkClient.ApproveAssignmentAsync(new ApproveAssignmentRequest
                    {
                        AssignmentId = assignment.AssignmentId
                    });
                }
            }

            return RedirectToAction("Index", "Images");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveAllHITsResults()
        {
            var hits = await _context.Image_HIT
                .Include(ih => ih.Image)
                .Where(ih => ih.Finished == false)
                .ToListAsync();

            foreach (var hit in hits)
            {
                var hitResponse = await _mturkClient.GetHITAsync(new GetHITRequest
                {
                    HITId = hit.MTurkID
                });

                var assignmentsResponse = await _mturkClient.ListAssignmentsForHITAsync(new ListAssignmentsForHITRequest
                {
                    HITId = hit.MTurkID,
                    AssignmentStatuses = new List<string>() { "Approved" }
                });

                if (assignmentsResponse.Assignments.Count() == 0)
                    continue;

                // Mark the hit as finished if no assignments left
                if (hitResponse.HIT.NumberOfAssignmentsAvailable == 0 && hitResponse.HIT.NumberOfAssignmentsPending == 0)
                {
                    hit.Finished = true;
                    _context.Update(hit);
                }

                if (hit.Type == Models.Type.Creation)
                {
                    List<int> BoundingBoxesTop = new List<int>();
                    List<int> BoundingBoxesBottom = new List<int>();
                    List<int> BoundingBoxesLeft = new List<int>();
                    List<int> BoundingBoxesRight = new List<int>();

                    XNamespace ns = "http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2005-10-01/QuestionFormAnswers.xsd";
                    foreach (var assignment in assignmentsResponse.Assignments)
                    {
                        var answerXML = XDocument.Parse(assignment.Answer);
                        var answerJSON = answerXML.Descendants(ns + "Answer").First().Element(ns + "FreeText").Value;

                        var answers = JsonConvert.DeserializeObject<List<AnswerJSON>>(answerJSON);

                        var boundingBox = answers.First().AnnotatedResult.BoundingBoxes.First();
                        BoundingBoxesTop.Add(boundingBox.Top);
                        BoundingBoxesBottom.Add(boundingBox.Top + boundingBox.Height);
                        BoundingBoxesLeft.Add(boundingBox.Left);
                        BoundingBoxesRight.Add(boundingBox.Left + boundingBox.Width);
                    }

                    System.Drawing.Image image = System.Drawing.Image.FromFile(Path.Combine(_he.ContentRootPath, "wwwroot/Images/NoBoundingBox/", hit.Image.ImageLocation));

                    // Filter results with high discrepancy
                    int unfilteredAverageTop = Convert.ToInt32(BoundingBoxesTop.Average());
                    int unfilteredAverageBottom = Convert.ToInt32(BoundingBoxesBottom.Average());
                    int unfilteredAverageLeft = Convert.ToInt32(BoundingBoxesLeft.Average());
                    int unfilteredAverageRight = Convert.ToInt32(BoundingBoxesRight.Average());

                    var heightTreshold = image.Height * 0.25;
                    var widthThreshold = image.Width * 0.25;

                    var FilteredBoundingBoxesTop = BoundingBoxesTop.Where(bb => bb <= unfilteredAverageTop + heightTreshold && bb >= unfilteredAverageTop - heightTreshold).ToList();
                    var FilteredBoundingBoxesBottom = BoundingBoxesBottom.Where(bb => bb <= unfilteredAverageBottom + heightTreshold && bb >= unfilteredAverageBottom - heightTreshold).ToList();
                    var FilteredBoundingBoxesLeft = BoundingBoxesLeft.Where(bb => bb <= unfilteredAverageLeft + widthThreshold && bb >= unfilteredAverageLeft - widthThreshold).ToList();
                    var FilteredBoundingBoxesRight = BoundingBoxesRight.Where(bb => bb <= unfilteredAverageRight *+ widthThreshold && bb >= unfilteredAverageRight - widthThreshold).ToList();

                    hit.Image.BoundingBoxTop = Convert.ToInt32(FilteredBoundingBoxesTop.Average());
                    hit.Image.BoundingBoxBottom = Convert.ToInt32(FilteredBoundingBoxesBottom.Average());
                    hit.Image.BoundingBoxLeft = Convert.ToInt32(FilteredBoundingBoxesLeft.Average());
                    hit.Image.BoundingBoxRight = Convert.ToInt32(FilteredBoundingBoxesRight.Average());

                    // Create a new JPG image with the bounding box for the confirmation 2nd phase task
                    using (Graphics g = Graphics.FromImage(image))
                    {
                        Pen pen = new Pen(Brushes.Red)
                        {
                            Width = 8.0F
                        };

                        Point TopLeft = new Point(Convert.ToInt32(hit.Image.BoundingBoxLeft), Convert.ToInt32(hit.Image.BoundingBoxTop));
                        Point TopRight = new Point(Convert.ToInt32(hit.Image.BoundingBoxRight), Convert.ToInt32(hit.Image.BoundingBoxTop));
                        Point BottomLeft = new Point(Convert.ToInt32(hit.Image.BoundingBoxLeft), Convert.ToInt32(hit.Image.BoundingBoxBottom));
                        Point BottomRight = new Point(Convert.ToInt32(hit.Image.BoundingBoxRight), Convert.ToInt32(hit.Image.BoundingBoxBottom));

                        g.DrawLine(pen, TopLeft, TopRight);
                        g.DrawLine(pen, TopRight, BottomRight);
                        g.DrawLine(pen, BottomLeft, BottomRight);
                        g.DrawLine(pen, TopLeft, BottomLeft);
                    }

                    image.Save(Path.Combine(_he.ContentRootPath, "wwwroot\\Images\\WithBoundingBox\\") + hit.Image.Id.ToString() + ".jpg");

                    _context.Image.Update(hit.Image);
                    await _context.SaveChangesAsync();
                }
                else if (hit.Type == Models.Type.Confirmation)
                {
                    int noCount = 0;

                    XNamespace ns = "http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2005-10-01/QuestionFormAnswers.xsd";
                    foreach (var assignment in assignmentsResponse.Assignments)
                    {
                        var answerXML = XDocument.Parse(assignment.Answer);
                        var answer = answerXML.Descendants(ns + "Answer").First().Element(ns + "FreeText").Value;

                        if (answer.ToUpper() == "NO")
                            noCount++;
                    }

                    // If there are a considerable number of "No" answers, then we consider the bounding box wrong, since the high number of "No" most likely means a problem in the bounding box
                    if (noCount >= assignmentsResponse.Assignments.Count() * 0.3)
                    {
                        hit.Image.BoundingBoxIsCorrect = null;
                        hit.Image.BoundingBoxBottom = null;
                        hit.Image.BoundingBoxLeft = null;
                        hit.Image.BoundingBoxRight = null;
                        hit.Image.BoundingBoxTop = null;
                    }
                    else
                        hit.Image.BoundingBoxIsCorrect = true;

                    _context.Image.Update(hit.Image);
                    await _context.SaveChangesAsync();
                }
                else
                    continue;
            }

            return RedirectToAction("Index", "Images");
        }

        public async Task<IActionResult> SaveAssignmentResults(string id)
        {
            if (id == null)
                return NotFound();

            var hitResponse = await _mturkClient.GetHITAsync(new GetHITRequest
            {
                HITId = id
            });

            var assignmentsResponse = await _mturkClient.ListAssignmentsForHITAsync(new ListAssignmentsForHITRequest
            { 
                HITId = id,
                AssignmentStatuses = new List<string> { "Approved" }
            });

            if (assignmentsResponse.Assignments.Count() == 0)
                return RedirectToAction(nameof(Details), new { id });

            var imageRelation = await _context.Image_HIT
                .Include(ir => ir.Image)
                .FirstAsync(ir => ir.MTurkID == id);

            // Mark the hit as finished if no assignments left
            if (hitResponse.HIT.NumberOfAssignmentsAvailable == 0 && hitResponse.HIT.NumberOfAssignmentsPending == 0)
            {
                imageRelation.Finished = true;
                _context.Update(imageRelation);
            }
                
            List<int> BoundingBoxesTop = new List<int>();
            List<int> BoundingBoxesBottom = new List<int>();
            List<int> BoundingBoxesLeft = new List<int>();
            List<int> BoundingBoxesRight = new List<int>();

            XNamespace ns = "http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2005-10-01/QuestionFormAnswers.xsd";
            foreach (var assignment in assignmentsResponse.Assignments)
            {
                var answerXML = XDocument.Parse(assignment.Answer);
                var answerJSON = answerXML.Descendants(ns + "Answer").First().Element(ns + "FreeText").Value;

                var answers = JsonConvert.DeserializeObject<List<AnswerJSON>>(answerJSON);

                var boundingBox = answers.First().AnnotatedResult.BoundingBoxes.First();
                BoundingBoxesTop.Add(boundingBox.Top);
                BoundingBoxesBottom.Add(boundingBox.Top + boundingBox.Height);
                BoundingBoxesLeft.Add(boundingBox.Left);
                BoundingBoxesRight.Add(boundingBox.Left + boundingBox.Width);
            }

            // Filter results with high discrepancy
            int unfilteredAverageTop = Convert.ToInt32(BoundingBoxesTop.Average());
            int unfilteredAverageBottom = Convert.ToInt32(BoundingBoxesBottom.Average());
            int unfilteredAverageLeft = Convert.ToInt32(BoundingBoxesLeft.Average());
            int unfilteredAverageRight = Convert.ToInt32(BoundingBoxesRight.Average());

            var FilteredBoundingBoxesTop = BoundingBoxesTop.Where(bb => bb < unfilteredAverageTop * 1.5 && bb > unfilteredAverageTop * 0.75).ToList();
            var FilteredBoundingBoxesBottom = BoundingBoxesBottom.Where(bb => bb < unfilteredAverageBottom * 1.5 && bb > unfilteredAverageBottom * 0.75).ToList();
            var FilteredBoundingBoxesLeft = BoundingBoxesLeft.Where(bb => bb < unfilteredAverageLeft * 1.5 && bb > unfilteredAverageLeft * 0.75).ToList();
            var FilteredBoundingBoxesRight = BoundingBoxesRight.Where(bb => bb < unfilteredAverageRight * 1.5 && bb > unfilteredAverageRight * 0.75).ToList();

            imageRelation.Image.BoundingBoxTop = Convert.ToInt32(FilteredBoundingBoxesTop.Average());
            imageRelation.Image.BoundingBoxBottom = Convert.ToInt32(FilteredBoundingBoxesBottom.Average());
            imageRelation.Image.BoundingBoxLeft = Convert.ToInt32(FilteredBoundingBoxesLeft.Average());
            imageRelation.Image.BoundingBoxRight = Convert.ToInt32(FilteredBoundingBoxesRight.Average());

            // Create a new JPG image with the bounding box for the confirmation 2nd phase task
            System.Drawing.Image image = System.Drawing.Image.FromFile(Path.Combine(_he.ContentRootPath, "wwwroot/Images/NoBoundingBox/", imageRelation.Image.ImageLocation));

            using (Graphics g = Graphics.FromImage(image))
            {
                Pen pen = new Pen(Brushes.Red)
                {
                    Width = 8.0F
                };

                Point TopLeft = new Point(Convert.ToInt32(imageRelation.Image.BoundingBoxLeft), Convert.ToInt32(imageRelation.Image.BoundingBoxTop));
                Point TopRight = new Point(Convert.ToInt32(imageRelation.Image.BoundingBoxRight), Convert.ToInt32(imageRelation.Image.BoundingBoxTop));
                Point BottomLeft = new Point(Convert.ToInt32(imageRelation.Image.BoundingBoxLeft), Convert.ToInt32(imageRelation.Image.BoundingBoxBottom));
                Point BottomRight = new Point(Convert.ToInt32(imageRelation.Image.BoundingBoxRight), Convert.ToInt32(imageRelation.Image.BoundingBoxBottom));

                g.DrawLine(pen, TopLeft, TopRight);
                g.DrawLine(pen, TopRight, BottomRight);
                g.DrawLine(pen, BottomLeft, BottomRight);
                g.DrawLine(pen, TopLeft, BottomLeft);
            }

            image.Save(Path.Combine(_he.ContentRootPath, "wwwroot\\Images\\WithBoundingBox\\") + imageRelation.Image.Id.ToString() + ".jpg");

            _context.Image.Update(imageRelation.Image);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Details), new { id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCreationHIT(int id)
        {
            var image = await _context.Image
                .Include(i => i.RelationHIT)
                .FirstAsync(i => i.Id == id);

            var question = System.IO.File.ReadAllText("XML\\HTML.xml");
            string questionCopy = question.Replace("~ImageURL~", image.Url);
            questionCopy = questionCopy.Replace("~PartType~", image.Classification.ToString());

            CreateHITResponse hitResponse = await _mturkClient.CreateHITAsync(new CreateHITRequest
            {
                Title = "Car Part Classification",
                Description = "Car Part Classification",
                Reward = "0.01",
                AssignmentDurationInSeconds = 60 * 60,
                LifetimeInSeconds = 60 * 60 * 24,
                Question = questionCopy
            });

            _context.Add(new Image_HIT
            {
                Image = image,
                MTurkID = hitResponse.HIT.HITId,
                Type = Models.Type.Creation,
                CreationDate = DateTime.Now
            });

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Details), new { id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateBoundingBoxHITs()
        { 
            var images = await _context.Image
                .Include(x => x.RelationHIT)
                .Where(x => x.BoundingBoxBottom == null && x.RelationHIT.Where(rh => rh.Finished == false && rh.Type == Models.Type.Creation).Count() == 0)
                .ToListAsync();

            var question = System.IO.File.ReadAllText("XML\\HTML.xml");

            foreach (Models.Image image in images)
            { 
                string questionCopy = question.Replace("~ImageURL~", image.Url);
                questionCopy = questionCopy.Replace("~PartType~", image.Classification.ToString());

                CreateHITResponse hitResponse = await _mturkClient.CreateHITAsync(new CreateHITRequest
                {
                    Title = "Car Part Classification",
                    Description = "Car Part Classification",
                    Reward = "0.01",
                    MaxAssignments = 5,
                    AssignmentDurationInSeconds = 60 * 60,
                    LifetimeInSeconds = 60 * 60 * 24,
                    Question = questionCopy
                });

                _context.Add(new Image_HIT
                {
                    Image = image,
                    MTurkID = hitResponse.HIT.HITId,
                    Type = Models.Type.Creation,
                    Finished = false,
                    CreationDate = DateTime.Now
                });
            }

            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Images");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateConfirmationHITs()
        {
            var images = await _context.Image
                .Include(x => x.RelationHIT)
                .Where(x => x.BoundingBoxBottom != null && x.BoundingBoxIsCorrect != true && x.RelationHIT.Where(rh => rh.Finished == false && rh.Type == Models.Type.Confirmation).Count() == 0)
                .ToListAsync();

            var fileTransfer = new TransferUtility(_s3Client);

            foreach (var image in images)
            {
                await fileTransfer.UploadAsync(Path.Combine(_he.ContentRootPath, "wwwroot\\Images\\WithBoundingBox\\") + image.Id.ToString() + ".jpg", _bucketName);

                var question = System.IO.File.ReadAllText("XML\\Confirmation.xml");
                string questionCopy = question.Replace("~ImageURL~", "https://" + _bucketName + ".s3.eu-west-3.amazonaws.com/" + image.Id.ToString() + ".jpg");
                questionCopy = questionCopy.Replace("~PartType~", image.Classification.ToString());

                CreateHITResponse hitResponse = await _mturkClient.CreateHITAsync(new CreateHITRequest
                {
                    Title = "Car Part Classification",
                    Description = "Car Part Classification",
                    Reward = "0.01",
                    AssignmentDurationInSeconds = 60 * 60,
                    LifetimeInSeconds = 60 * 60 * 24,
                    MaxAssignments = 5,
                    Question = questionCopy
                });

                _context.Add(new Image_HIT
                {
                    Image = image,
                    MTurkID = hitResponse.HIT.HITId,
                    Type = Models.Type.Confirmation,
                    Finished = false,
                    CreationDate = DateTime.Now
                });
            }
          
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Images");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateBoundingBoxConfirmationHIT(int id)
        {
            ModelState.Clear();

            if (ModelState.IsValid)
            {
                var image = await _context.Image
                    .Include(i => i.RelationHIT)
                    .FirstOrDefaultAsync(i => i.Id == id);

                if (image == null)
                    return RedirectToAction(nameof(Index));

                if (image.BoundingBoxBottom == null || image.BoundingBoxLeft == null || image.BoundingBoxRight == null || image.BoundingBoxTop == null || image.BoundingBoxIsCorrect == true || image.RelationHIT.Where(rh => rh.Type == Models.Type.Confirmation && rh.Finished == false).Count() != 0)
                    return RedirectToAction("Details", "Images", new { id });

                var fileTransfer = new TransferUtility(_s3Client);

                await fileTransfer.UploadAsync(Path.Combine(_he.ContentRootPath, "wwwroot\\Images\\WithBoundingBox\\") + image.Id.ToString() + ".jpg", _bucketName);

                var question = System.IO.File.ReadAllText("XML\\Confirmation.xml");
                string questionCopy = question.Replace("~ImageURL~", "https://" + _bucketName + ".s3.eu-west-3.amazonaws.com/" + image.Id.ToString() + ".jpg");
                questionCopy = questionCopy.Replace("~PartType~", image.Classification.ToString());

                CreateHITResponse hitResponse = await _mturkClient.CreateHITAsync(new CreateHITRequest
                {
                    Title = "Car Part Classification",
                    Description = "Car Part Classification",
                    Reward = "0.01",
                    AssignmentDurationInSeconds = 60 * 60,
                    LifetimeInSeconds = 60 * 60 * 24,
                    Question = questionCopy
                });

                _context.Add(new Image_HIT
                {
                    Image = image,
                    MTurkID = hitResponse.HIT.HITId,
                    Type = Models.Type.Confirmation,
                    Finished = false
                });

                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Details", "Images", new { id });
        }

        public async Task<IActionResult> SaveBoundingBoxConfirmationResults(string id)
        {
            if (id == null)
                return NotFound();

            var hitResponse = await _mturkClient.GetHITAsync(new GetHITRequest
            {
                HITId = id
            });

            var assignmentsResponse = await _mturkClient.ListAssignmentsForHITAsync(new ListAssignmentsForHITRequest
            {
                HITId = id,
                AssignmentStatuses = new List<string> { "Approved" }
            });

            if (assignmentsResponse.Assignments.Count() == 0)
                return RedirectToAction(nameof(Details), new { id });

            var imageRelation = await _context.Image_HIT
                .Include(ir => ir.Image)
                .FirstAsync(ir => ir.MTurkID == id);

            // Mark the hit as finished if no assignments left
            if (hitResponse.HIT.NumberOfAssignmentsAvailable == 0 && hitResponse.HIT.NumberOfAssignmentsPending == 0)
            {
                imageRelation.Finished = true;
                _context.Update(imageRelation);
            }

            int noCount = 0;

            XNamespace ns = "http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2005-10-01/QuestionFormAnswers.xsd";
            foreach (var assignment in assignmentsResponse.Assignments)
            {
                var answerXML = XDocument.Parse(assignment.Answer);
                var answer = answerXML.Descendants(ns + "Answer").First().Element(ns + "FreeText").Value;

                if (answer.ToUpper() == "NO")
                    noCount++;
            }

            // If there are a considerable number of "No" answers, then we consider the bounding box wrong, since the high number of "No" most likely means a problem in the bounding box
            if (noCount >= assignmentsResponse.Assignments.Count() * 0.3)
            {
                imageRelation.Image.BoundingBoxIsCorrect = null;
                imageRelation.Image.BoundingBoxBottom = null;
                imageRelation.Image.BoundingBoxLeft = null;
                imageRelation.Image.BoundingBoxRight = null;
                imageRelation.Image.BoundingBoxTop = null;
            }
            else
                imageRelation.Image.BoundingBoxIsCorrect = true;

            _context.Image.Update(imageRelation.Image);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Details), new { id });
        }
    }
}
