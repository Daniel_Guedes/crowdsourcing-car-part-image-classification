﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CarPartImageClassification.Models;

namespace CarPartImageClassification.Data
{
    public class CarPartImageClassificationContext : DbContext
    {
        public CarPartImageClassificationContext (DbContextOptions<CarPartImageClassificationContext> options)
            : base(options)
        {
        }

        public DbSet<CarPartImageClassification.Models.Image> Image { get; set; }

        public DbSet<CarPartImageClassification.Models.Image_HIT> Image_HIT { get; set; }
    }
}
