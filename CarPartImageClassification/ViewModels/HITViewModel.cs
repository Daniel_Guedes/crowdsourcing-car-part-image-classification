﻿using Amazon.MTurk.Model;
using CarPartImageClassification.Models;
using System.ComponentModel.DataAnnotations;

namespace CarPartImageClassification.ViewModels
{
    public class HITViewModel
    {
        [Display(Name = "HIT")]
        public HIT HIT { get; set; }

        [Display(Name = "Imagem")]
        public Image Image { get; set; }

        [Display(Name = "Tipo de HIT")]
        public Models.Type TypeHIT { get; set; }
    }
}
