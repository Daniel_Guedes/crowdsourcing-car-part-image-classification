﻿using CarPartImageClassification.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarPartImageClassification.ViewModels
{
    public class IndexImageViewModel
    {
        [Display(Name = "Imagens")]
        public IList<Image> Images { get; set; }

        public bool AwaitingConfirmation { get; set; }
        public bool AwaitingBoundingBox { get; set; }
        public bool AwaitingCreation { get; set; }
        public bool HITsInProgress { get; set; }

    }
}
