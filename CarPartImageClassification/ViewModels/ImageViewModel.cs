﻿using CarPartImageClassification.Models;
using System.Collections.Generic;

namespace CarPartImageClassification.ViewModels
{
    public class ImageViewModel
    {
        public Image Image { get; set; }
        public List<Image_HIT> CreationHITs { get; set; }
        public List<Image_HIT> ConfirmationHITs { get; set; }
    }
}
