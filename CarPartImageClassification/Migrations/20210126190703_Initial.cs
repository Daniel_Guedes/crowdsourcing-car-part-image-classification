﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CarPartImageClassification.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Image",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(nullable: false),
                    ImageLocation = table.Column<string>(nullable: false),
                    Classification = table.Column<int>(nullable: false),
                    BoundingBoxTop = table.Column<int>(nullable: true),
                    BoundingBoxBottom = table.Column<int>(nullable: true),
                    BoundingBoxLeft = table.Column<int>(nullable: true),
                    BoundingBoxRight = table.Column<int>(nullable: true),
                    BoundingBoxIsCorrect = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Image_HIT",
                columns: table => new
                {
                    MTurkID = table.Column<string>(nullable: false),
                    ImageId = table.Column<int>(nullable: false),
                    Finished = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image_HIT", x => x.MTurkID);
                    table.ForeignKey(
                        name: "FK_Image_HIT_Image_ImageId",
                        column: x => x.ImageId,
                        principalTable: "Image",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Image_HIT_ImageId",
                table: "Image_HIT",
                column: "ImageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Image_HIT");

            migrationBuilder.DropTable(
                name: "Image");
        }
    }
}
