﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarPartImageClassification.Models
{
    public enum Classificacao
    {
        [Display(Name = "Front Light")]
        FrontLight,
        [Display(Name = "Back Light")]
        BackLight,
        [Display(Name = "Fog Light")]
        FogLight,
        [Display(Name = "Steering Wheel")]
        SteeringWheel,
        [Display(Name = "Center Console")]
        Console,
        [Display(Name = "Instrument Panel")]
        InstrumentPanel,
        [Display(Name = "Speedometer")]
        Speedometer,
        [Display(Name = "Gear Handle")]
        GearHandle,
        [Display(Name = "Grill")]
        Grill
    }


    public class Image
    {
        [Key]
        [Required]  
        public int Id { get; set; }

        /**
         * URL of the image.
         */
        [Required]
        public string Url { get; set; }

        /**
         * Image location in the filesystem.
         */
        [Required]
        public string ImageLocation { get; set; }

        /**
         * Classification of the image.
         */
        [Required]
        [Display(Name = "Classificação")]
        public Classificacao Classification { get; set; }

        /**
         * Y coordinate of the top border of the bounding box.
         */
        public int? BoundingBoxTop { get; set; }

        /**
         * Y coordinate of the bottom border of the bounding box.
         */
        public int? BoundingBoxBottom { get; set; }

        /**
         * X coordinate of the left border of the bounding box.
         */
        public int? BoundingBoxLeft { get; set; }

        /**
         * X coordinate of the right border of the bounding box.
         */
        public int? BoundingBoxRight { get; set; }

        /**
         * Confirmation if the given bounding box is correct. 
         */
        [Display(Name = "Bounding Box Correta")]
        public bool? BoundingBoxIsCorrect { get; set; }

        /**
         * HITs associated with the image.
         */
        public List<Image_HIT> RelationHIT { get; set; }
    }
}
