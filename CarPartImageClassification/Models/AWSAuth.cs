﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPartImageClassification.Models
{
    public class AWSAuth
    {
        public string AccessKeyID { get; set; }
        public string SecretKey { get; set; }
    }
}
