﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPartImageClassification.Models
{
    public class BoundingBox
    {
        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "height")]
        public int Height { get; set; }

        [JsonProperty(PropertyName = "left")]
        public int Left { get; set; }

        [JsonProperty(PropertyName = "top")]
        public int Top { get; set; }

        [JsonProperty(PropertyName = "width")]
        public int Width { get; set; }
    }

    public class InputImageProperties
    {
        [JsonProperty(PropertyName = "height")]
        public int Heigth { get; set; }

        [JsonProperty(PropertyName = "width")]
        public int Width { get; set; }
    }


    public class ArrayElement
    {
        [JsonProperty(PropertyName = "boundingBoxes")]
        public List<BoundingBox> BoundingBoxes { get; set; }

        [JsonProperty(PropertyName = "inputImageProperties")]
        public InputImageProperties InputImageProperties { get; set; }
    }

    public class AnswerJSON
    {
        [JsonProperty(PropertyName = "annotatedResult")]
        public ArrayElement AnnotatedResult { get; set; }

    }
}
