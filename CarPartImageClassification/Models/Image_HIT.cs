﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarPartImageClassification.Models
{
    public enum Type
    {
        [Display(Name = "Criação de Bounding Box")]
        Creation,
        [Display(Name = "Confirmação de Bounding Box")]
        Confirmation
    }

    public class Image_HIT
    {
        [Key]
        [Required]
        [Display(Name = "HIT")]
        public string MTurkID { get; set; }

        [Required]
        [Display(Name = "Imagem")]
        public Image Image { get; set; }
       
        [Required]
        [Display(Name = "Finalizado")]
        public bool Finished { get; set; }

        [Required]
        [Display(Name = "Tipo")]
        public Type Type { get; set; }

        [Required]
        [Display(Name = "Data de Criação")]
        public DateTime CreationDate { get; set; }
    }
}
